<div align="center">
  <img alt="Postman" src="https://img.shields.io/badge/postman-Ff0000?style=for-the-badge&logo=coffeescript&logoColor=white">
  <img alt="Newman" src="https://img.shields.io/badge/newman-Ff0000?style=for-the-badge&logo=coffeescript&logoColor=white">
</div>


# Bootcamp Sicredi - Testes manuais de API do DBC Bank☕

Repositório para alocar os testes manuais de API que foram desenvolvidos durante o projeto do DBC Bank.

## Links
- [Swagger da API]()


## Pré-requisitos ⚙️

- [Postman](https://www.postman.com/downloads/)
- [Newman Report](https://www.npmjs.com/package/newman-reporter-htmlextra)



## Ferramentas e Tecnologias Utilizadas

- **Postman**: plataforma de desenvolvimento de API que permite testar, desenvolver e colaborar em APIs de maneira eficiente.
- **Newman Report**: uma ferramenta de linha de comando para executar coleções de testes do Postman e gerar relatórios detalhados sobre essas execuções.
